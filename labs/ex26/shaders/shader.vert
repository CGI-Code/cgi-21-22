attribute vec4 vPosition;
attribute vec3 vNormal;

uniform mat4 mProjection;
uniform mat4 mModelView;
uniform mat4 mNormals;
uniform int uMapMode;

varying vec3 fNormal;
varying vec2 fTexCoord;

#define ORTHO_XY    0
#define ORTHO_XZ    1
#define ORTHO_ZY    2
#define SPHERICAL   3
#define CYLINDRIC   4

#define PI acos(-1.0)

vec2 sphericalMap(vec3 v) {
    float alpha = atan(-v.z, v.x);
    float r = sqrt(v.x * v.x + v.z * v.z);
    float theta = atan(v.y,r);

    float s = (alpha+PI)/(2.0*PI);
    float t = (theta + PI/2.0)/PI;
    return vec2(s, t);
}


vec2 cylindricalMap(vec3 v) {
    float alpha = atan(-v.z, v.x);
    float s = (alpha+PI)/(2.0*PI);
    float t = v.y + 0.5; 
    return vec2(s,t);
}
// Assignment of texture coordinates to vertices based on their location and mapping mode
vec2 textureMap(vec3 v, int mode)
{
    if(mode == ORTHO_XY)
        return v.xy + vec2(0.5, 0.5);
    else if(mode == ORTHO_XZ)
        return v.xz + vec2(0.5, 0.5);
    else if(mode == ORTHO_ZY)
        return v.zy + vec2(0.5, 0.5);
    else if(mode == SPHERICAL)
        return sphericalMap(v);
    else if(mode == CYLINDRIC)
        return cylindricalMap(v);
    else return vec2(0.0, 0.0);
}

void main()
{
    gl_Position = mProjection * mModelView * vPosition;
    fNormal = (mNormals * vec4(vNormal, 0.0)).xyz;
    fTexCoord = textureMap(vPosition.xyz, uMapMode);
}